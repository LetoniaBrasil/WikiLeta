+++
fragment = "nav"
#disabled = true
date = "2019-27-09"
weight = 0
background = "dark"
search = true
sticky = true

[breadcrumb]
  display = true # Default value is false
  #level = 0 # Default is 1
  background = "light"

# Branding options
[asset]
  image = "logo.svg"
  text = "GPP"

[repo_button]
  url = "https://github.com/okkur/syna"
  text = "Repository" 
  icon = "fab fa-gitlab" 
+++
