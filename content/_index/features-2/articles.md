+++
title = "Articles"
weight = 10

[asset]
  icon = "fas fa-pencil-alt"
+++

All data is accompanied by articles explaining the significance and history of the artifact.
