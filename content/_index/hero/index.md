+++
fragment = "hero"
#disabled = true
date = "2019-27-09"
weight = 50
background = "light" # can influence the text color
particles = true

title = "Latvian Brazilian Wiki"
subtitle = "Gaismas Pils Project"

[header]
  image = "header.jpg"

[asset]
  image = "logo.svg"
  width = "500px" # optional - will default to image width
  height = "150px" # optional - will default to image height

#[[buttons]]
#  text = "Video Archive"
#  url = "#"
#  color = "info" # primary, secondary, success, danger, warning, info, light, dark, link - #default: primary

#[[buttons]]
#  text = "Wiki"
#  url = "https://github.com/okkur/syna/releases"
#  color = "primary"

#[[buttons]]
#  text = "Photo Arhive"
#  url = "#"
#  color = "success"
+++
