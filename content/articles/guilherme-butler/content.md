+++
fragment = "content"
weight = 100

title = "# Guilherme Butler"

[sidebar]
  sticky = true
+++
# About
<img style="padding-left: 20px; float: right;" src="./guilherme_butler_1.jpg" alt="Guilherme Butler em Curitiba"
	title="Guilherme Butler em Curitiba" width="40%"/>

_Vilis Butlers_, also known in Brazil as Guilherme Butler (born in Jelgava, 15 July 1880 - Curitiba, 13 February 1963) was a Latvian Explorer, Geographer, Professor, Theologian and Anthropologue famous for his many expeditions to explore different parts of Brazil during the 1930’s and 1950’s. He was also a prominent writer in many Brazilian newspapers, who published his accounts. He also played a part in the formation of Brazil's airforce.

---
## Early Life

Vilis Butler (also written sometimes as Vilhelms or Wilhelms) was born in Jelgava, son of the serf Peteris (? - Rio Novo, 14 March 1922) and Magrieta (Grieta) (? - Rio Novo, 30 November 1914). He had three sisters (Katrine, and two other yet to be identified). He studied in Jelgavas Gymnasium during his youth and possibly learned German and was a member of Jelgava’s Baptist Church in 1899.<sup>[1](#references)</sup>

It is not exactly known the origins of Butler’s family and his surname. It is known that Butler before coming to Brazil Vilis spoke german well, although there is no evidence to point them as a german or baltic-german family. Other theory suggested the Butler family being an english or scottish merchant family established in Latvia, but this contradicts with Peteris serf status. Many newspapers in Brazil wrongly identified him as English or Danish.

---
## Professor in Rio Novo

The Rio Novo colony was founded in 1889, and the construction of the Second Baptist Church was completed in 1897, but there was no organized school. The Latvian missionary stationed in Rio Novo João Inkis started looking for teachers. The first option was his cousing Karlis Inkis, but due to his studies in Saint Petersburg, could not take the position. After returning to Latvia, Inkis convinced his young professor friend Vilis Butler.<sup>[2](#references)</sup>

Butler arrived in Brazil in May 1900, followed soon by his brother-in-law Alexandre Klavin, in July. He soon started studying portuguese, and started reorganizing the school system, introducing a complete reform and a new curriculum for a 6 years study program. His plans also involved the construction of a new school building which could serve as school, baptist temple and house for the teacher.

His enthusiasm and dynamism soon caught the attention of both baptist and lutheran latvians, as well as other german and italian of neighboring colonies. He also promoted a vast divulgation of his school reforms in other latvian colonies in Brazil, such as Ijui, Mãe Luzia e Blumenau. He constantly emphasized the necessity to prepare the young generation to modern times. 

His school curriculum was very well prepared and prestigious: Latvian, Portuguese, German, Mathematics, Algebra, Geometry, Geography, History of the World, History of Brazil, Natural History, Physics, Drawing, Calligraphy, Sacred History, Ecclesiastic History, Gymnastics, Domestic Arts for girls and Music, including violin and harmonica. At this time, he also published the first Latvian-Portuguese dictionary, _Portugalu Valodas Mahciba_<sup>[3](#references)</sup>

In 1901, Butler was also approached by the german baptist missionary Karl Roth (_Carlos Roth_), of the The German Baptist Missionary Society of Philadelphia, to open a Dominical School for the german colonists in the area.

He also planned a boarding school for students from outside the colony, which soon started applying<sup>[2](#references)</sup>. The final school building was officialy inaugurated in 24 June 1902.

In his years there, the Latvian School became so famous that in 1903 the Governor of the State of Santa Catarina, [Vidal Ramos](https://en.wikipedia.org/wiki/Vidal_Ramos_(politician)) decided to visit it. His committee was receptioned by the school choir. The event was followed by a baptist cult and speech delivered by Butler, followed again by the choir. The visiting committee was reported to be very emotional, and the governor delivered an impromptu speech about the "industrious spirit" of the Latvian people, declaring the school one of the best of the State. In this same event, the mayor of Rio Novo promised a monthly 30$000 réis funding for the school, which fulfilled while Butler was there.

---
## Living in United States

In 1903, the incumbent baptist pastor of Rio Novo, Alexandre Klavin, left to study in Karl Roth's recently founded baptist seminary in Porto Alegre. The Baptist Church then elected Guilherme Butler to be the supplementary pastor. In the end of 1903, Butler decided to study formally study theology. He left Brazil and enrolled in the  German Academy of Rochester, in the United States. From 1910 to 1912, he ingressed in The Newton Theological Institution, in the University of New York, and completed his studies in the University of Columbia, in 1913. From 1910 to 1913, he was the evangelist of several german churches and became pastor of the Baptist Church of New York<sup>[2](#references)</sup>.

---
## Rio Novo

Butler returned to Rio Novo in late 1913, where he met Marta Andermane (Ligatnes Skudrukalns, 23 February 1896 - Curitiba, 10 April 1985), whom he would marry in November 1st, 1916. In February 1914, he represented Rio Novo in the Baptist Conference in Rio de Janeiro. Later that year he began working as a professor in the Baptist College in Rio de Janeiro. In Rio de Janeiro he met the missionary A.B. Dieter. In 1917 Butler also helped the Baptist church of Rio Branco.

Butler left Rio de Janeiro and returned to Rio Novo in March 1919 due to medical reasons. He became sick in 1914 (possibly Tuberculosis) and his disease became worse in 1915. In Rio Novo he assumed pastorship of the Baptist Church together with Roberto Klavin and the direction of the School. During his years in the United States, the Baptist Church of Rio Novo suffered a number of theological and political divisions. Thanks to his annual visits in summer and letter correspondence, Butler was able to impede a bigger fragmentation. In 11 May 1919, he reunified the Orleans and Rio Novo Baptist Churches.

Together with A.B. Dieter, he founded the Baptist Association of Paraná-Santa Catarina (later Baptist Association of Paraná). In the day of the reunification the Rio Novo church affiliated itself with the Association. In the first assembly, on 10-13 of June 1919, Butler was elected president. His presidency helped shape education in southern Brazil, as all baptist churches began building adjacent schools<sup>[2](#references)</sup>.

During 1918-1920, Butler organized several charity funds through the International Red Cross to help the war-torn Latvia.

## Life in Curitiba

In 1920, A.B. Dieter ended his mission in the First Baptist Church of Curitiba and invited Butler to be pastor. In 10 August 1920, he applied for a position as professor of German and English in Paraná’s Gymnasium (today Colégio Estadual do Paraná), a position he competed with other candidates, including a candidate supported by the catholic bishop, Joaquim Penido Monteiro. In 1921 he was appointed professor by the governor Dr. Caetano Munhoz da Rocha.

During this decade Butler dedicated his years between helping the Baptist church in Brazil and his academic teaching. In 1926, he and Ricardo J. Inkis were considered possible teachers to open a school in the recently founded latvian colony in Varpa. He visited the colony again in 1932, but could not move because of his professional obligations.
 
In Curitiba, he had his only daughter, Helen Anne Butler. 

In 1930, he published a book called “England and the United States”, a collection of poems, authors and books to help the education of english in Brazil. The house in which he lived,  Ratcliff street, 110 (today, Rua Desembargador Westphalen, 1014), is a historical patrimony of Curitiba.

## First Expeditions

A Brazilian patriot and man of science, Butler saw the lack of integration between different regions of Brazil, as well as educational faults in way brazilians perceived their own culture and country. After years of saving funds, Butler planned to anthropological expeditions through different regions of Brazil, which he could use for his studies and help educate the public.

His first expedition was to the Amazon Rainforest in 1934. Followed by Mato Grosso (1935), Minas Gerais (1936), Bacia do Rio da Prata (1937) and Goiás (1938). Described by newspapers as a “Colonel Fawcett”-type, Butler rose to national prominence by writing multiple articles of history, geography and culture of each region. His mastery of portuguese and elaborate speech made him a charismatic figure of the decade.

[Author’s note: to be expanded later]
 
## World War II

With Brazil joining the Second World War in 1942, Butler enlisted in the Brazilian Air Force as teacher and examiner of the pilots, which needed to learn English to cooperate with other Allied forces, a job he fulfilled until the end of the war. It is not known if Butler had helped the army before, but he was now an official member of the Armed Forces.

It is reported that Butler was naturalized a Brazilian citizen during the Second World War, although this information conflicts with his application as a professor in 1920, which required applicants to be Brazilian citizens.

## Later Expeditions

After 30 years as a professor, Butler retired in 14 December 1950. He gained the title of Paranymph and his farewell speech “The traits of an educated person” was published entirely in the newspapers.

From 1950 to 1957, he continued his expeditions in Brazil, now with the logistical help of the Brazilian Air Force and funding by the Minister of Aeronautics Marshal Eduardo Gomes. 

During these years he also published numerous articles in Latvian newspapers (such as _Kristigs Draugs_, _Latviesi Paranas dienvidos_ and _Latviesu Kalendars_). In 1957 he became a member of the Geography and History Institutes of Pará and Amazonias. 

In 1959 he was named honorary citizen of Curitiba and receive a street in his name.

## Death and Legacy

Guilherme Butler died in 13 February 1962, aged 82, due to complete cardiac arrest. His death was widely reported through Brazil and Latvia, and attended by hundreds of people in Curitiba.

---
# References
1. LATVIEŠI PASAULĒ - MUZEJS UN PĒTNIECĪBAS CENTRS, LP2016.477. [Link](http://www.meandrs.lv/viewObjectInstanceForm.jsp?pk=270003381823&1524916788589)
2. Uma Epopéia de Fé. Pg. 114, 117, 118, 119, 120, 124, 125.
3. Butlers, W., Portugalu valodas mahciba (O ensino da lingua portuguesa), Rio Novo,1901, Printed with W. Rotermund, S. Leopoldo, p. 32. Found in the Museu Batista do Seminário Teológico Batista do Sul do Brasil, Rio de Janeiro, GB
